package cyanoHABpredictor;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.junit.*;


//テストを完成させる
public class CyanoHABsPredictorTest {
	
	String drive = "E";
	
	final String Florida = drive+"lorida";
	final String Ohio = "Ohio";
	final String NE = "New_England";
	CyanoHABsPredictor solution = new CyanoHABsPredictor();

	// TODO activeId(predictionに渡すか決めるやつ)
	HashSet<Integer> activeId ;

	StringBuilder nextareaMerisLine;

	

	@Before
	public void setUp() {
		activeId =  new HashSet<>();
	}

	private int getDay(int id){
		return id%365-1;
	}
	private int getYear(int id) {
		return id/365+2009;
	}

	private int getDay(StringBuilder sb) {
		return getDay(sb.toString());
	}

	private  int getDay(String line){
		//System.out.println("getDay");
		//System.out.println(line + "line");
		if(line.length() == 0 || line == null)return -1;
		return Integer.parseInt(line.split(",")[1]);
	}

	private int getYear(String line) {
		if(line == null || line.length() == 0)return -1;
		return Integer.parseInt(line.split(",")[0]);
	}

	private  HashMap<Integer,char[][]> makeHash(BufferedReader br) throws IOException {
		String line = "";
		line = br.readLine();//一行目を飛ばす
		line = br.readLine();
		int day = getDay(line);
		int year = getYear(line);
		HashMap<Integer,char[][]> map = new HashMap<>();
		while(true) {
			List<char[]> list = new ArrayList<>();
			while (true) {
				list.add(line.toCharArray());
				if((line = br.readLine()) == null) break;
				if(day != getDay(line))break;
			}
			if(line == null)return map;

			int key = day+year*1000;
			//System.out.println(key);


			day = getDay(line);

		}
	}


	private  String[][] getData(BufferedReader br, int day, int year, String[] data0, String[] data1,StringBuilder sb) throws IOException {
		//System.out.println("getData");
		//System.out.println(getDay(data0[0]));
		if(getDay(data0[0]) == day){
			String[] res = data0;
			data0 = data1;
			data1 = nextData(br,sb);


			//System.out.println(data0[0]+" "+data1[0]);
			return new String[][]{res,data0,data1};
		};

		return new String[][]{null,data0,data1};
	}


	private String[] nextData(BufferedReader br,StringBuilder sb) throws IOException{
		List<String> list = new ArrayList<>();
		String line = (sb.length() == 0) ? br.readLine() :sb.toString();

		list.add(line);

		int day = -1;
		while((line = br.readLine())  != null) {
			//System.out.println("aaa"+line);

			int d = Integer.parseInt(line.split(",")[1]);
			//System.out.println(d);
			if(d != day && day != -1){
				break;
			}
			if (day == -1)day = d;

			list.add(line);
			//System.out.println(day +" " + d);
		}
		sb.setLength(0);
		sb.append(line);

		return (String[])list.toArray(new String[0]);
	}

	@Test
	public void testcase_0() {
		
		int testnum = 0;

		String region = "";
		double minVInterested = -1;
		double maxVInterested = -1;
		int timeSpan = 7;
		int learningFirstYear = 2009;
		int learningFirstDay = 32;
		int learningLastYear = 2010;
		int learningLastDay = 31;

		int testFirstYear = 2010;
		int testFirstDay = 32;
		int testLastYear = 2010;
		int testLastDay = 31+28+31+30+31+30+31;

		int numberOfTestCase= -1;

		try {
			File csv;
			String line;

			//control読み込む
			csv = new File(drive+":\\Training_Data_Streams\\Test_Case_"+testnum+"_Control.csv");
			BufferedReader brcon = new BufferedReader(new FileReader(csv));
			region = brcon.readLine();
			minVInterested = Double.parseDouble(brcon.readLine());
			maxVInterested = Double.parseDouble(brcon.readLine());
			timeSpan = Integer.parseInt(brcon.readLine());
			int testFirstDateId = Integer.parseInt(brcon.readLine());
			testFirstDay = getDay(testFirstDateId);
			testFirstYear = getDay(testFirstDateId);
			int testLastDateID = Integer.parseInt(brcon.readLine());
			testLastDay = getDay(testLastDateID);
			testLastYear = getDay(testLastDateID);
			numberOfTestCase = Integer.parseInt(brcon.readLine());


			BufferedReader area = new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\"+region+"_Area_Info.csv")));



			//areaInfoの読み込み
			List<String> arealist = new ArrayList<>();
			while ((line = area.readLine() ) != null) {
				arealist.add(line);
			}
			String[] areaInfo =	(String[]) arealist.toArray(new String[0]);
			//for(String s: areaInfo)System.out.println(s);

			/*----- init呼び出し -----*/
			solution.init(region, minVInterested, maxVInterested, timeSpan, areaInfo);


			/*---- recieveData呼び出し　-----*/

			BufferedReader meris= new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\"+region+"_Meris.csv")));
			BufferedReader areameris= new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\"+region+"_Ground_Truth.csv")));

			areameris.readLine();
			nextareaMerisLine = new StringBuilder("");
			String[] areamerisData0 = nextData(areameris,nextareaMerisLine);
			System.out.println(nextareaMerisLine.toString());

			String[] areamerisData1 = nextData(areameris,nextareaMerisLine);
			System.out.println(nextareaMerisLine.toString() + "NEXTMERISnohazu");

			boolean f = false;
			if(f)return;

			for(int year = learningFirstYear; year <= learningLastYear; year++){
				int fd = (year == learningFirstYear) ? learningFirstDay: 1;
				int ld = (year == learningLastYear) ? learningLastDay: 365;
				for(int dayOfYear = fd ;dayOfYear  <= ld; dayOfYear++){

					//String key = Integer.toString(year)+" "+Integer.toString(dayOfYear);

					String[][] areamerisDataarr = getData(areameris, dayOfYear, year, areamerisData0, areamerisData1, nextareaMerisLine);
					areamerisData0 = areamerisDataarr[1];
					areamerisData1 = areamerisDataarr[2];
					String[] merisData = null;
					//System.out.println(getDay(nextMerisLine));
					String[] areaMerisData = areamerisDataarr[0];
					String[] globalClimate = null;
					String[] localClimate = null;
					String[] waterQuality = null;
					String[] cropScape = null;

					solution.receiveData(dayOfYear,year , merisData, areaMerisData, globalClimate, localClimate, waterQuality, cropScape);
				}

				
			}


			/*---- areaMerisdataの中身の表示　----*/
			//System.out.println(Arrays.toString(solution.merisMap.get(4)));


			/*----- ここからpredict受け取る ------*/
			BufferedReader brreq = new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\Test_Case_"+testnum+"_Requests.csv")));
			brreq.readLine();
			System.out.println(brcon.readLine());
			String reqRangeStr;
			int index = 1;
			double totalScore = 0;

			while ((reqRangeStr = brcon.readLine()) != null){

				String[] el = reqRangeStr.split(",");
				int reqstart = index;
				int reqEnd = Integer.parseInt( el[2] );
				int day = getDay( Integer.parseInt(el[0]));
				int year = getYear(Integer.parseInt(el[0]));
				System.out.println("reqEnd:"+reqEnd);
				System.out.println("reqStart"+reqstart);
				if(reqEnd == 0)continue;
				int[] requests = new int[reqEnd-reqstart];
				double[] results = new double[reqEnd-reqstart];
				for (; index < reqEnd; index++) {
					line = brreq.readLine();
					int  areaname = Integer.parseInt(line.split(",")[1]);
					double result = Double.parseDouble(line.split(",")[2]);

					requests[index-reqstart] = areaname;
					results[index-reqstart] = result;
				}





				double[] predicts = solution.predict(day, year, timeSpan, requests);
				System.out.println("predicts recieved");
				if(predicts == null){
					System.out.println("nullpointeruntarakantara");
					return;
				}
				if(predicts.length != requests.length) {
					System.out.println("長さが違う");
					return;
				}

				for(int i = 0; i < predicts.length; i++) {
					double p = predicts[i];
					double r = results[i];
					System.out.println(p+ " "+r);
					double score = (p < 10.000)?0:1-Math.pow(Math.log10(p)-Math.log10(r),2);
					score*= 1_000_000;
					totalScore+=score;
				}
				System.out.println(totalScore);
			}

			assertSame(totalScore, (double)1_000_000*numberOfTestCase);

		}  catch (FileNotFoundException e) {
			// Fileオブジェクト生成時の例外捕捉
			e.printStackTrace();
		} catch (IOException e) {
			// BufferedReaderオブジェクトのクローズ時の例外捕捉
			e.printStackTrace();
		}


	}
	

	
	@Test
	public void testcase_1() {
		
		int testnum = 1;

		String region = "";
		double minVInterested = -1;
		double maxVInterested = -1;
		int timeSpan = 7;
		int learningFirstYear = 2009;
		int learningFirstDay = 32;
		int learningLastYear = 2010;
		int learningLastDay = 31;

		int testFirstYear = 2010;
		int testFirstDay = 32;
		int testLastYear = 2010;
		int testLastDay = 31+28+31+30+31+30+31;

		int numberOfTestCase= -1;

		try {
			File csv;
			String line;

			//control読み込む
			csv = new File(drive+":\\Training_Data_Streams\\Test_Case_"+testnum+"_Control.csv");
			BufferedReader brcon = new BufferedReader(new FileReader(csv));
			region = brcon.readLine();
			minVInterested = Double.parseDouble(brcon.readLine());
			maxVInterested = Double.parseDouble(brcon.readLine());
			timeSpan = Integer.parseInt(brcon.readLine());
			int testFirstDateId = Integer.parseInt(brcon.readLine());
			testFirstDay = getDay(testFirstDateId);
			testFirstYear = getDay(testFirstDateId);
			int testLastDateID = Integer.parseInt(brcon.readLine());
			testLastDay = getDay(testLastDateID);
			testLastYear = getDay(testLastDateID);
			numberOfTestCase = Integer.parseInt(brcon.readLine());



			BufferedReader area = new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\"+region+"_Area_Info.csv")));



			//areaInfoの読み込み
			List<String> arealist = new ArrayList<>();
			while ((line = area.readLine() ) != null) {
				arealist.add(line);
			}
			String[] areaInfo =	(String[]) arealist.toArray(new String[0]);
			//for(String s: areaInfo)System.out.println(s);

			/*----- init呼び出し -----*/
			solution.init(region, minVInterested, maxVInterested, timeSpan, areaInfo);


			/*---- recieveData呼び出し　-----*/

			BufferedReader meris= new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\"+region+"_Meris.csv")));
			BufferedReader areameris= new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\"+region+"_Ground_Truth.csv")));

			areameris.readLine();
			nextareaMerisLine = new StringBuilder("");
			String[] areamerisData0 = nextData(areameris,nextareaMerisLine);
			System.out.println(nextareaMerisLine.toString());

			String[] areamerisData1 = nextData(areameris,nextareaMerisLine);
			System.out.println(nextareaMerisLine.toString() + "NEXTMERISnohazu");

			boolean f = false;
			if(f)return;

			for(int year = learningFirstYear; year <= learningLastYear; year++){
				int fd = (year == learningFirstYear) ? learningFirstDay: 1;
				int ld = (year == learningLastYear) ? learningLastDay: 365;
				for(int dayOfYear = fd ;dayOfYear  <= ld; dayOfYear++){

					//String key = Integer.toString(year)+" "+Integer.toString(dayOfYear);

					String[][] areamerisDataarr = getData(areameris, dayOfYear, year, areamerisData0, areamerisData1, nextareaMerisLine);
					areamerisData0 = areamerisDataarr[1];
					areamerisData1 = areamerisDataarr[2];
					String[] merisData = null;
					//System.out.println(getDay(nextMerisLine));
					String[] areaMerisData = areamerisDataarr[0];
					String[] globalClimate = null;
					String[] localClimate = null;
					String[] waterQuality = null;
					String[] cropScape = null;

					solution.receiveData(dayOfYear,year , merisData, areaMerisData, globalClimate, localClimate, waterQuality, cropScape);
				}

			}


			/*---- areaMerisdataの中身の表示　----*/
			//System.out.println(Arrays.toString(solution.merisMap.get(4)));


			/*----- ここからpredict受け取る ------*/
			BufferedReader brreq = new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\Test_Case_"+testnum+"_Requests.csv")));
			brreq.readLine();
			System.out.println(brcon.readLine());
			String reqRangeStr;
			int index = 1;
			double totalScore = 0;

			while ((reqRangeStr = brcon.readLine()) != null){

				String[] el = reqRangeStr.split(",");
				int reqstart = index;
				int reqEnd = Integer.parseInt( el[2] );
				int day = getDay( Integer.parseInt(el[0]));
				int year = getYear(Integer.parseInt(el[0]));
				System.out.println("reqEnd:"+reqEnd);
				System.out.println("reqStart"+reqstart);
				if(reqEnd == 0)continue;
				int[] requests = new int[reqEnd-reqstart];
				double[] results = new double[reqEnd-reqstart];
				for (; index < reqEnd; index++) {
					line = brreq.readLine();
					int  areaname = Integer.parseInt(line.split(",")[1]);
					double result = Double.parseDouble(line.split(",")[2]);

					requests[index-reqstart] = areaname;
					results[index-reqstart] = result;
				}





				double[] predicts = solution.predict(day, year, timeSpan, requests);
				System.out.println("predicts recieved");
				if(predicts == null){
					System.out.println("nullpointeruntarakantara");
					return;
				}
				if(predicts.length != requests.length) {
					System.out.println("長さが違う");
					return;
				}

				for(int i = 0; i < predicts.length; i++) {
					double p = predicts[i];
					double r = results[i];
					System.out.println(p+ " "+r);
					double score = (p < 10.000)?0:1-Math.pow(Math.log10(p)-Math.log10(r),2);
					score*= 1_000_000;
					totalScore+=score;
				}
				System.out.println(totalScore);
			}

			assertSame(totalScore,(double)1_000_000*numberOfTestCase);


		}  catch (FileNotFoundException e) {
			// Fileオブジェクト生成時の例外捕捉
			e.printStackTrace();
		} catch (IOException e) {
			// BufferedReaderオブジェクトのクローズ時の例外捕捉
			e.printStackTrace();
		}






	}
	
	
	
	@Test
	public void testcase_2() {
		
		int testnum = 2;

		String region = "";
		double minVInterested = -1;
		double maxVInterested = -1;
		int timeSpan = 7;
		int learningFirstYear = 2009;
		int learningFirstDay = 32;
		int learningLastYear = 2010;
		int learningLastDay = 31;

		int testFirstYear = 2010;
		int testFirstDay = 32;
		int testLastYear = 2010;
		int testLastDay = 31+28+31+30+31+30+31;

		int numberOfTestCase= -1;

		try {
			File csv;
			String line;

			//control読み込む
			csv = new File(drive+":\\Training_Data_Streams\\Test_Case_"+testnum+"_Control.csv");
			BufferedReader brcon = new BufferedReader(new FileReader(csv));
			region = brcon.readLine();
			minVInterested = Double.parseDouble(brcon.readLine());
			maxVInterested = Double.parseDouble(brcon.readLine());
			timeSpan = Integer.parseInt(brcon.readLine());
			int testFirstDateId = Integer.parseInt(brcon.readLine());
			testFirstDay = getDay(testFirstDateId);
			testFirstYear = getDay(testFirstDateId);
			int testLastDateID = Integer.parseInt(brcon.readLine());
			testLastDay = getDay(testLastDateID);
			testLastYear = getDay(testLastDateID);
			numberOfTestCase = Integer.parseInt(brcon.readLine());



			BufferedReader area = new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\"+region+"_Area_Info.csv")));



			//areaInfoの読み込み
			List<String> arealist = new ArrayList<>();
			while ((line = area.readLine() ) != null) {
				arealist.add(line);
			}
			String[] areaInfo =	(String[]) arealist.toArray(new String[0]);
			//for(String s: areaInfo)System.out.println(s);

			/*----- init呼び出し -----*/
			solution.init(region, minVInterested, maxVInterested, timeSpan, areaInfo);


			/*---- recieveData呼び出し　-----*/

			BufferedReader meris= new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\"+region+"_Meris.csv")));
			BufferedReader areameris= new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\"+region+"_Ground_Truth.csv")));

			areameris.readLine();
			nextareaMerisLine = new StringBuilder("");
			String[] areamerisData0 = nextData(areameris,nextareaMerisLine);
			System.out.println(nextareaMerisLine.toString());

			String[] areamerisData1 = nextData(areameris,nextareaMerisLine);
			System.out.println(nextareaMerisLine.toString() + "NEXTMERISnohazu");

			boolean f = false;
			if(f)return;

			for(int year = learningFirstYear; year <= learningLastYear; year++){
				int fd = (year == learningFirstYear) ? learningFirstDay: 1;
				int ld = (year == learningLastYear) ? learningLastDay: 365;
				for(int dayOfYear = fd ;dayOfYear  <= ld; dayOfYear++){

					//String key = Integer.toString(year)+" "+Integer.toString(dayOfYear);

					String[][] areamerisDataarr = getData(areameris, dayOfYear, year, areamerisData0, areamerisData1, nextareaMerisLine);
					areamerisData0 = areamerisDataarr[1];
					areamerisData1 = areamerisDataarr[2];
					String[] merisData = null;
					//System.out.println(getDay(nextMerisLine));
					String[] areaMerisData = areamerisDataarr[0];
					String[] globalClimate = null;
					String[] localClimate = null;
					String[] waterQuality = null;
					String[] cropScape = null;

					solution.receiveData(dayOfYear,year , merisData, areaMerisData, globalClimate, localClimate, waterQuality, cropScape);
				}

			}


			/*---- areaMerisdataの中身の表示　----*/
			//System.out.println(Arrays.toString(solution.merisMap.get(4)));


			/*----- ここからpredict受け取る ------*/
			BufferedReader brreq = new BufferedReader(new FileReader(new File(drive+":\\Training_Data_Streams\\Test_Case_"+testnum+"_Requests.csv")));
			brreq.readLine();
			System.out.println(brcon.readLine());
			String reqRangeStr;
			int index = 1;
			double totalScore = 0;

			while ((reqRangeStr = brcon.readLine()) != null){

				String[] el = reqRangeStr.split(",");
				int reqstart = index;
				int reqEnd = Integer.parseInt( el[2] );
				int day = getDay( Integer.parseInt(el[0]));
				int year = getYear(Integer.parseInt(el[0]));
				System.out.println("reqEnd:"+reqEnd);
				System.out.println("reqStart"+reqstart);
				if(reqEnd == 0)continue;
				int[] requests = new int[reqEnd-reqstart];
				double[] results = new double[reqEnd-reqstart];
				for (; index < reqEnd; index++) {
					line = brreq.readLine();
					int  areaname = Integer.parseInt(line.split(",")[1]);
					double result = Double.parseDouble(line.split(",")[2]);

					requests[index-reqstart] = areaname;
					results[index-reqstart] = result;
				}





				double[] predicts = solution.predict(day, year, timeSpan, requests);
				System.out.println("predicts recieved");
				if(predicts == null){
					System.out.println("nullpointeruntarakantara");
					return;
				}
				if(predicts.length != requests.length) {
					System.out.println("長さが違う");
					return;
				}

				for(int i = 0; i < predicts.length; i++) {
					double p = predicts[i];
					double r = results[i];
					System.out.println(p+ " "+r);
					double score = (p < 10.000)?0:1-Math.pow(Math.log10(p)-Math.log10(r),2);
					score*= 1_000_000;
					totalScore+=score;
				}
				System.out.println(totalScore);
			}

			double actual = (double)((double)1_000_000*(double)numberOfTestCase);
			assertSame(totalScore,actual);


		}  catch (FileNotFoundException e) {
			// Fileオブジェクト生成時の例外捕捉
			e.printStackTrace();
		} catch (IOException e) {
			// BufferedReaderオブジェクトのクローズ時の例外捕捉
			e.printStackTrace();
		}






	}
	
	

}

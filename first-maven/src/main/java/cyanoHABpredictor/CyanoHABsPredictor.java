package cyanoHABpredictor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;


public class CyanoHABsPredictor {
	private String region;
	private double minVInterested;
	private double maxVInterested;
	private int timeSpan;
	private String[] areaInfo;
	private HashMap<Integer,double[]> areaIdtoCordinate = new HashMap<>();

	private svm svm = new svm();
	private svm_model model;


	private HashMap<Integer,double[]> AMMap = new HashMap<>();
	private HashMap<Integer,double[]> logAMMap = new HashMap<>();
	private HashMap<Integer,double[]> logMerisMap = new HashMap<>();


	public int init(String region, double minVInterested, double maxVInterested, int timeSpan, String[] areaInfo){

		this.region = region;
		this.minVInterested = minVInterested;
		this.maxVInterested = maxVInterested;
		this.areaInfo = areaInfo;


		System.out.println(region + " " + minVInterested + " " + maxVInterested );

		for(String info: areaInfo) {
			String[] el = info.split(",");
			if(el[5].equals("1")){//active or not
				int areaId = Integer.parseInt(el[0]);

				AMMap.put(areaId, new double[367]);
				logAMMap.put(areaId, new double[367]);

				double minLat = Double.parseDouble(el[1]);
				double maxLat = Double.parseDouble(el[2]);
				double minLon = Double.parseDouble(el[3]);
				double maxLon = Double.parseDouble(el[4]);

				double[] cordinate = {(minLat+maxLat)/2,(minLon+maxLon)/2};

				areaIdtoCordinate.put(areaId, cordinate);
			}
		}

		System.out.println(AMMap.size());
		return -1;
	}



	public int receiveData(
			int dayOfYear, int year, String[] merisData, String[] areaMerisData,
			String[] globalClimate,String[] localClimate, String[] waterQuality, String[] cropScape
			) {

		if(areaMerisData != null){
			for(String data: areaMerisData) {
				String[] el = data.split(",");
				int areaId = Integer.parseInt(el[2]);
				double cons = Double.parseDouble(el[3]);
				if(AMMap.get(areaId) == null){
					continue;
				}
				AMMap.get(areaId)[dayOfYear] = cons;
				logAMMap.get(areaId)[dayOfYear] = Math.log10(cons);
				//System.out.println(areaId +" "+ dayOfYear +" "+ cons);
			}

		}

		System.out.println("data of " + dayOfYear + " " + year +"recieved!");
		return -1;
	}

	boolean flat = false;
	//TODO 使わないならhasCurve除去

	private boolean isPredicted = false;

	public double[] predict(int day, int year, int timeSpan, int[] requests){
		int N = requests.length;
		double[] res = new double[N];

		if(flat){
			Arrays.fill(res, 10232.9);
			return res;
		}


		if(isPredicted) {//fitting
			isPredicted = true;



			/*------*/
			List<double[]> AMX = new ArrayList<double[]>();
			List<Double> AMY = new ArrayList<Double>();
			for(Map.Entry<Integer,double[]> ent:areaIdtoCordinate.entrySet()){
				int id = ent.getKey();
				double[] cord = ent.getValue();

				double y = logAMMap.get(id)[day];
				double[] x = {day,cord[0],cord[1]};
				AMX.add(x);
				AMY.add(y);
			}

			List<double[]> merisX = new ArrayList<double[]>();
			List<Double> merisY = new ArrayList<Double>();



			svm_problem prob = new svm_problem();
			prob.l = AMMap.size();


			svm_node[][] x = new svm_node[merisX.size()+AMX.size()][3];
			double[] y = new double[merisX.size()+AMX.size()];
			for (int i = 0; i < merisX.size(); i++) {
				double[] v = merisX.get(i);
				svm_node[]  vector = new svm_node[3];
				for(int j = 0; j < 3; j++){
					vector[j].value = v[j];
					vector[j].index = j+1;
				}

				x[i] = vector;
				y[i] = merisY.get(i);
			}

			for (int i = 0; i < AMX.size(); i++) {
				double[] v = AMX.get(i);
				svm_node[]  vector = new svm_node[3];
				for(int j = 0; j < 3; j++){
					vector[j].value = v[j];
					vector[j].index = j+1;
				}

				x[i+merisX.size()] = vector;
				y[i+merisX.size()] = AMY.get(i);
			}

			svm_parameter param = new svm_parameter();

			model = svm.svm_train(prob, param);



		}


		/*
		for(int i = 0; i < N; i++) {
			int areaId = requests[i];
			double[] merisData = logAMMap.get(areaId);
			int j = day;
			List<Double> plist = new ArrayList<>();
			while(j-day+1 <= timeSpan){

				j++;
				if(j <= 0)j = 366;
				if(merisData[j] != 0)plist.add(merisData[j]);
			}
			double sum = 0;
			for(Double p:plist){
				sum+=p;
			}
			res[i] = (sum == 0)?10232.9:Math.pow(10,sum/plist.size());

			if(res[i] < 10232.9)res[i] = 10232.9;
			if(res[i] > maxVInterested)res[i] = maxVInterested;
		}
*/

		for(int i = 0; i < N; i++) {
			int id = requests[i];



			double amData = svm.svm_predict(model, ) ;
		}

		System.out.println("predict of " + day + " " + year +"is done");
		System.out.println(Arrays.toString(res));
		return res;
	}

}



class Area {

	double latitude;
	double longtitude;
	int areaId;

}
